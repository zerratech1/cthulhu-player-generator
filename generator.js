// JSON postaci

const person = `{
    "gender":"",
    "name":"",
    "surname":"",
    "nickname":"",
    "birthplace":"",
    "languageOwn":"",
    "residence":"",
    "occupation":"",
    "age":0,
    "str":0,
    "con":0,
    "siz":0,
    "dex":0,
    "app":0,
    "edu":0,
    "int":0,
    "pow":0,
    "moveRate":0,
    "hp":0,
    "sanity":0,
    "luck":0,
    "magePoints":0,
    "damageBonus" : 0,
    "build" : 0,
    "skills" : {}
  }`

//skills of every investigator
const skillTab = [
  ["accounting", 5],
  ["anthropology", 1],
  ["appraise", 5],
  ["archeology", 1],
  ["art/craft", 5,""],
  ["charm", 15],
  ["climb", 20],
  ["computer use",5],
  ["credit rating", 0],
  ["cthulhu mythos", 0],
  ["demolitions",1],
  ["disguise", 5],
  ["diving",1],
  ["dodge", 0/*dex*/],
  ["drive auto", 20],
  ["electrical repair", 10],
  ["fast talk", 5],
  ["fighting", 25, "brawl"],
  ["firearms", 15, "bow"],
  ["first aid", 30],
  ["history",5],
  ["hypnosis",1],
  ["intimidate",15],
  ["jump",20],
  ["language",0/*edu*/,""],
  ["law",5],
  ["library use",20],
  ["listen",20],
  ["locksmith",1],
  ["lore",1],
  ["mechanical repair",10],
  ["medicine",1],
  ["natural world",10],
  ["navigate",10],
  ["occult",5],
  ["operate heavy machinery",1],
  ["persuade",10],
  ["pilot",1],
  ["psychoanalysis",1],
  ["psychology",10],
  ["read lips",1],
  ["ride",5],
  ["science",1,""],
  ["sleight of hand",10],
  ["spot hidden",25],
  ["stealth",20],
  ["survival",10],
  ["swim",20],
  ["throw",20],
  ["track",10],
  ["fighting",15,"axe"],
  ["fighting",10,"chainsaw"],
  ["fighting",10,"flail"],
  ["fighting",15,"garrote"],
  ["fighting",20,"spear"],
  ["fighting",20,"sword"],
  ["fighting",5,"whip"],
  ["firearms",10,"flamethrower"],
  ["firearms",20,"handgun"],
  ["firearms",10,"heavy weapons"],
  ["firearms",10,"machine gun"],
  ["firearms",25,"rifle"],
  ["firearms",25,"shotgun"],
  ["firearms",15,"submachine gun"]
];


const occupationTab = [
  {
    name:"tester",
    creditRating:[30,70],
    occSkillPoints:[5],
    occSkillList:["interpersonal","interpersonal","interpersonal","interpersonal", ["random",], ["random",], ["random",], ["random",]],
    minAge: 15,
    maxAge: 89
  },
  {
    name:"Antiquarian",
    creditRating:[30,70],
    occSkillPoints:[5],
    occSkillList:["appraise", ["art/craft", "random"], "history", "library use", ["other language",], "interpersonal", "spot hidden", ["random",]],
    minAge: 40,
    maxAge: 89
  },
  {
    name:"Artist",
    creditRating:[9,50],
    occSkillPoints:[5,3,7],
    occSkillList:[["art/craft", "random"],["choose","history","natural world"],"interpersonal", ["other language",],"psychology","spot hidden",["random",],["random",]],
    minAge: 15,
    maxAge: 89
  },
  {
    name:"Athlete",
    creditRating:[9,70],
    occSkillPoints:[5,0,3],
    occSkillList:["climb","jump",["fighting","brawl"],"ride","interpersonal","swim","throw",["random",]],
    minAge: 15,
    maxAge: 49
  },
  {
    name:"Author",
    creditRating:[9,30],
    occSkillPoints:[5],
    occSkillList:[["art/craft", "literature"],"history", "library use", ["choose", "natural world", "occult"],["other language",],"language", "psychology", ["random",]],
    minAge: 25,
    maxAge: 89
  },
  {
    name:"Member of the clergy",
    creditRating:[9,60],
    occSkillPoints:[5],
    occSkillList:["accounting", "history", "library use", "listen", ["other language",], "interpersonal", "psychology", ["random",]],
    minAge: 30,
    maxAge: 89
  },
  {
    name:"Criminal",
    creditRating:[5,65],
    occSkillPoints:[5,0,3],
    occSkillList:["interpersonal", "psychology", "spot hidden", "stealth", ["choose", "appraise", "disguise", ["fighting",""], ["firearms",""], "locksmith", "mechanical repair", "sleight of hand"],["choose", "appraise", "disguise", ["fighting",""], ["firearms",""], "locksmith", "mechanical repair", "sleight of hand"],["choose", "appraise", "disguise", ["fighting",""], ["firearms",""], "locksmith", "mechanical repair", "sleight of hand"],["choose", "appraise", "disguise", ["fighting",""], ["firearms",""], "locksmith", "mechanical repair", "sleight of hand"]],
    minAge: 15,
    maxAge: 49
  },
  {
    name:"Dilettante",
    creditRating:[50,99],
    occSkillPoints:[5,4],
    occSkillList:[["art/craft", "random"],["choose",["firearms",""]], ["other language",],"ride", "interpersonal", ["random",], ["random",], ["random",]],
    minAge: 20,
    maxAge: 59
  },
  {
    name:"Doctor of medicine",
    creditRating:[30,80],
    occSkillPoints:[5],
    occSkillList:["first aid", ["language", "latin"], "medicine", "psychology", ["science", "biology"],["science", "pharmacy"], ["random",], ["random",]],  //2 ostatnie umiejki miały być zależne od specjalizacji medycznej.. ale w sumie tylko psychoanaliza z puli skili do tego pasuje więc dałem random
    minAge: 35,
    maxAge: 89
  },
  {
    name:"Drifter",
    creditRating:[0,5],
    occSkillPoints:[5,0,3,4],
    occSkillList:["climb","jump","listen","navigate", "interpersonal", "stealth", ["random",], ["random",]],
    minAge: 15,
    maxAge: 59
  },
  {
    name:"Engineer",
    creditRating:[30,60],
    occSkillPoints:[5],
    occSkillList:[["art/craft", "technical drawing"], "electrical repair", "library use", "mechanical repair", "operate heavy machinery", ["science", "engineering"], ["science", "physics"], ["random",]],
    minAge: 20,
    maxAge: 49
  },
  {
    name:"Entertainer",
    creditRating:[9,70],
    occSkillPoints:[5,4],
    occSkillList:[["art/craft", "acting"],"disguise", "interpersonal", "interpersonal","listen", "psychology", ["random",], ["random",]],
    minAge: 15,
    maxAge: 89
  },
  {
    name:"Farmer",
    creditRating:[9,30],
    occSkillPoints:[5,0,3],
    occSkillList:[["art/craft", "farming"],"drive auto", "interpersonal", "mechanical repair", "natural world", "operate heavy machinery", "track", ["random",]], 
    minAge: 15,
    maxAge: 89
  },
  {
    name:"Hacker",
    creditRating:[10,70],
    occSkillPoints:[5],
    occSkillList:["computer use", "electrical repair", "electronics", "library use", "spot hidden", "interpersonal", ["random",], ["random",]],
    minAge: 15,
    maxAge: 49
  },
  {
    name:"Journalist",
    creditRating:[9,30],
    occSkillPoints:[5],
    occSkillList:[["art/craft", "photography"],"history","library use", ["language",], "interpersonal", "psychology", ["random",], ["random",]],
    minAge: 30,
    maxAge: 59
  },
  {
    name:"Lawyer",
    creditRating:[30,80],
    occSkillPoints:[5],
    occSkillList:["accounting", "law", "library use", "interpersonal", "interpersonal", "psychology", ["random",], ["random",]],
    minAge: 35,
    maxAge: 79
  },
  {
    name:"Librarian",
    creditRating:[9,35],
    occSkillPoints:[5],
    occSkillList:["accounting", "library use", ["other language",], ["language",], ["random",], ["random",], ["random",], ["random",]],
    minAge: 40,
    maxAge: 89
  },
  {
    name:"Military officer",
    creditRating:[20,70],
    occSkillPoints:[5,0,3],
    occSkillList:["accounting", ["firearms",""], "navigate", "interpersonal", "interpersonal", "psychology", "survival", ["random",]],
    minAge: 30,
    maxAge: 89
  },
  {
    name:"Missionary",
    creditRating:[0,30],
    occSkillPoints:[5],
    occSkillList:[["art/craft", "random"], "first aid", "mechanical repair", "medicine", "natural world", "interpersonal", ["random",], ["random",]],
    minAge: 25,
    maxAge: 89
  },
  {
    name:"Musician",
    creditRating:[9,30],
    occSkillPoints:[5,3,7],
    occSkillList:[["art/craft", "instrument"], "interpersonal", "listen", "psychology", ["random",], ["random",], ["random",], ["random",]],
    minAge: 15,
    maxAge: 89
  },
  {
    name:"Parapsychologist",
    creditRating:[9,30],
    occSkillPoints:[5],
    occSkillList:["anthropology", ["art/craft", "photography"], "history", "library use", "occult", ["other language",], "psychology", ["random",]],
    minAge: 30,
    maxAge: 89
  },
  {
    name:"Pilot",
    creditRating:[20,70],
    occSkillPoints:[5,3],
    occSkillList:["electrical repair", "navigate", "operate heavy machinery", "pilot", ["science", "astronomy"], ["random",], ["random",]],
    minAge: 25,
    maxAge: 59
  },
  {
    name:"Police detective",
    creditRating:[20,50],
    occSkillPoints:[5,0,3],
    occSkillList:[["choose", ["art/craft", "acting"], "disguise"], ["firearms",""], "first aid", "interpersonal", "psychology", "spot hidden", ["random",]],
    minAge: 20,
    maxAge: 59
  },
  {
    name:"Police officer",
    creditRating:[9,30],
    occSkillPoints:[5,0,3],
    occSkillList:[["fighting","brawl"], ["firearms",""], "first aid", "interpersonal", "law", "psychology", "spot hidden", ["choose", "drive", "ride"]],
    minAge: 40,
    maxAge: 69
  },
  {
    name:"Private investigator",
    creditRating:[9,30],
    occSkillPoints:[5,0,3],
    occSkillList:[["art/craft", "photography"], "disguise", "law", "library use", "interpersonal", "psychology", "spot hidden", ["random",]],
    minAge: 20,
    maxAge: 49
  },
  {
    name:"Professor",
    creditRating:[20,70],
    occSkillPoints:[5],
    occSkillList:["library use", ["other language",], ["language",], "psychology", ["random",], ["random",], ["random",], ["random",]],
    minAge: 40,
    maxAge: 89
  },
  {
    name:"Soldier",
    creditRating:[9,30],
    occSkillPoints:[5,0,3],
    occSkillList:[["choose", "climb", "swim"], "dodge", ["fighting",""], ["firearms",""], "stealth", "survival", ["choose", "first aid", "mechanical repair", ["other language",]], ["choose", "first aid", "mechanical repair", ["other language",]]],
    minAge: 20,
    maxAge: 49
  },
  {
    name:"Tribe member",
    creditRating:[0,15],
    occSkillPoints:[5,0,3],
    occSkillList:["climb", ["choose", ["fighting",""], "throw"], "natural world", "listen", "occult", "spot hidden", "swim", "survival"],
    minAge: 15,
    maxAge: 89
  },
  {
    name:"Zealot",
    creditRating:[0,30],
    occSkillPoints:[5,4,7],
    occSkillList:["history", "interpersonal", "interpersonal", "psychology", "stealth", ["random",], ["random",], ["random",]],
    minAge: 15,
    maxAge: 89
  },
];


let playerCharacter = JSON.parse(person);
playerCharacter.skills = skillTab;
let killerSkill;          //zmienna która przechowa ID credit rating do wyłączania skilli do dodawania
for (let i = 0; i < playerCharacter.skills.length; i++) {
  if (playerCharacter.skills[i][0]=="credit rating") {
    killerSkill = i;
    console.log(i);
  }
}
let randomSkill;
let randomSkillRoll;
const playerGender = Math.random()
const nameM = ["Michal", "Ariel", "Piotr", "Andrzej", "Szymon", "Drizzt", "Namir"];
const nameF = ["Agnieszka", "Aleksandra", "Karolina", "Stefania", "Sorsha", "Sindel", "Kitana"];
const playerSurname = ["Wheeler", "Wick", "Wayne", "Payne", "Quiet"];
const playerNickname = ["Snake", "Ocelote", "Raiden", "Ace", "Twitch"];
const playerBirthplace = ["Warsaw", "New York", "Lizbona", "Paris", "London"]; 
const playerResidence = ["Warsaw", "New York", "Lizbona", "Paris", "Tokyo"]; 
const playerLanguageOwn = ["polish", "english", "spanish", "french", "japanese", "latin"];
const artCraft = ["acting","fine art", "forgery", "photography", "literature", "technical drawing", "instrument"]
const science = ["astrology","biology","botany","chemistry","cryptography","engineering","forensics","geology","mathematics","meteorology","pharmacy","physics","zoology"]
const interpersonalTab = ["charm", "fast talk", "intimidate", "persuade"];
const ageTab = [0,0,1,1,1,2,2,3,4,5,6];
const ageRoller = ageTab[Math.floor(Math.random()*ageTab.length)];
const fightingTab = ["brawl", "axe", "chainsaw", "flail", "garrote", "spear", "sword", "whip"];
const firearmsTab = ["bow", "flamethrower", "handgun", "heavy weapons", "machine gun", "rifle", "shotgun", "submachine gun"];
//const ageRoller = 6;
let ageDeductFinisher = 0;


if (playerGender>0.5) {
  playerCharacter.gender = "M";
  playerCharacter.name = nameM[Math.floor(Math.random()*7)];
} else {
  playerCharacter.gender = "F";
  playerCharacter.name = nameF[Math.floor(Math.random()*7)];
}
playerCharacter.surname = playerSurname[Math.floor(Math.random()*5)];
playerCharacter.nickname = playerNickname[Math.floor(Math.random()*5)];
const birthRoll = Math.floor(Math.random()*5);
playerCharacter.birthplace = playerBirthplace[birthRoll];
playerCharacter.languageOwn = playerLanguageOwn[birthRoll];
playerLanguageOwn.splice(birthRoll,1);
playerCharacter.residence = playerResidence[Math.floor(Math.random()*5)];

playerCharacter.str = (3 * rollD6()) *5;
playerCharacter.con = (3 * rollD6()) *5;
playerCharacter.siz = (2 * rollD6() +6) *5;
playerCharacter.dex = (3 * rollD6()) *5;
playerCharacter.app = (3 * rollD6()) *5;
playerCharacter.edu = (2 * rollD6() +6) *5;
playerCharacter.int = (2 * rollD6() +6) *5;
playerCharacter.pow = (3 * rollD6()) *5;
playerCharacter.luck = (3 * rollD6()) *5;
playerCharacter.sanity = playerCharacter.pow;
playerCharacter.magePoints = playerCharacter.pow/5;
playerCharacter.hp = Math.floor((playerCharacter.con + playerCharacter.siz)/10);

//bonus Damage and build
const buildBonusDamage = playerCharacter.str+playerCharacter.siz
switch (true) {
  case (buildBonusDamage<64):
    playerCharacter.damageBonus = -2;
    playerCharacter.build = -2;
    break;
  case (buildBonusDamage<84):
    playerCharacter.damageBonus = -1;
    playerCharacter.build = -1;
    break;
  case (buildBonusDamage<124):
    playerCharacter.damageBonus = 0;
    playerCharacter.build = 0;
    break;
  case (buildBonusDamage<164):
    playerCharacter.damageBonus = "+1d4"
    playerCharacter.build = 1
    break;
  default:
    playerCharacter.damageBonus = "+1d6"
    playerCharacter.build = 2
    break;
}

// movement value
switch (true) {
  case (playerCharacter.dex<playerCharacter.siz && playerCharacter.str<playerCharacter.siz):
    playerCharacter.moveRate = 7;
    break;
  case (playerCharacter.dex>playerCharacter.siz && playerCharacter.str>playerCharacter.siz):
    playerCharacter.moveRate = 9;
    break;
  default:
    playerCharacter.moveRate = 8;
    break;
}

switch (ageRoller) {
  case 0:
    playerCharacter.age = Math.floor(Math.random()*5) + 15;
    const youngLuck = (3 * rollD6()) *5;
    playerCharacter.str -= 5;
    playerCharacter.siz -= 5;
    playerCharacter.edu -= 5;
    if (youngLuck>playerCharacter.luck) {
      playerCharacter.luck = youngLuck;
    }
    break;
  case 1:
    playerCharacter.age = Math.floor(Math.random()*20) + 20;
    eduSkillCheck(playerCharacter.edu);
    break;
  case 2:
    playerCharacter.age = Math.floor(Math.random()*10) + 40;
    playerCharacter.moveRate -=1;
    playerCharacter.app -=5;
    for (let i = 0; i < 2; i++) {
      eduSkillCheck(playerCharacter.edu);
    }
    ageDeduct(5);
    break;
  case 3:
    playerCharacter.age = Math.floor(Math.random()*10) + 50;
    playerCharacter.moveRate -=2;
    playerCharacter.app -=10;
    for (let i = 0; i < 3; i++) {
      eduSkillCheck(playerCharacter.edu);
    }
    ageDeduct(10);
    break;
  case 4:
    playerCharacter.age = Math.floor(Math.random()*10) + 60;
    playerCharacter.moveRate -=3;
    playerCharacter.app -=15;
    for (let i = 0; i < 4; i++) {
      eduSkillCheck(playerCharacter.edu);
    }
    ageDeduct(20);
    break;
  case 5:
    playerCharacter.age = Math.floor(Math.random()*10) + 70;
    playerCharacter.moveRate -=4;
    playerCharacter.app -=20;
    for (let i = 0; i < 4; i++) {
      eduSkillCheck(playerCharacter.edu);
    }
    ageDeduct(40);
    break;
  case 6:
    playerCharacter.age = Math.floor(Math.random()*10) + 80;
    playerCharacter.moveRate -=5;
    playerCharacter.app -=25;
    for (let i = 0; i < 4; i++) {
      eduSkillCheck(playerCharacter.edu);
    }
    if (playerCharacter.str+playerCharacter.dex+playerCharacter.con>=80) {
      ageDeduct(80);
      if (ageDeductFinisher>0) {
        ageDeduct(ageDeductFinisher);
      }
    } else {
      playerCharacter.str =0;
      playerCharacter.dex =0;
      playerCharacter.con =0;
      //window.alert("You died. Generate other character.");
    }
    break;
  default:
    break;
}
//warningi

if (playerCharacter.app<0) {
  playerCharacter.app = 0;
  window.alert("You ugly bitch");
}
if (playerCharacter.dex==0) {
  window.alert("You are uncordinated, you can't perform any physical tasks. Strongly recomend to generate other character.");
}
if (playerCharacter.str==0) {
  window.alert("Your strength is too low! You are unable to leave your bed! Generate other character.");
}
if (playerCharacter.con==0) {
  window.alert("You died. Generate other character.");
}

//update skillów
for (let i = 0; i < playerCharacter.skills.length; i++) {
  if (playerCharacter.skills[i][0] == "dodge") {
    playerCharacter.skills[i][1] = Math.floor(playerCharacter.dex/2);
  }
  if (playerCharacter.skills[i][0] == "language") {
    playerCharacter.skills[i][2]=playerCharacter.languageOwn;
playerCharacter.skills[i][1]=playerCharacter.edu;
  }
}
//playerCharacter.skills[12][1]=Math.floor(playerCharacter.dex/2);
//playerCharacter.skills[23][2]=playerCharacter.languageOwn;
//playerCharacter.skills[23][1]=playerCharacter.edu;

//zawód kurwa
const attributeTab = [
  playerCharacter.str,  //0
  playerCharacter.con,  //1
  playerCharacter.siz,  //2
  playerCharacter.dex,  //3
  playerCharacter.app,  //4
  playerCharacter.edu,  //5
  playerCharacter.int,  //6
  playerCharacter.pow,  //7
];
let playerSkillPoints = 0;

//przydział zawodu
let ageOccupationTab = [];
for (let i = 0; i < occupationTab.length; i++) {
  if (playerCharacter.age >= occupationTab[i].minAge && playerCharacter.age <= occupationTab[i].maxAge) {
    if (occupationTab[i].occSkillPoints.length>1) {
      if (occupationTab[i].occSkillPoints.length>2) {
        if (attributeTab[occupationTab[i].occSkillPoints[3]]>attributeTab[occupationTab[i].occSkillPoints[2]]) {
          attributeTab[occupationTab[i].occSkillPoints[2]]=attributeTab[occupationTab[i].occSkillPoints[3]];
        }
        if (attributeTab[occupationTab[i].occSkillPoints[1]]>attributeTab[occupationTab[i].occSkillPoints[2]]) {
          ageOccupationTab.push([i, attributeTab[occupationTab[i].occSkillPoints[0]]+attributeTab[occupationTab[i].occSkillPoints[1]]*2]);
        } else {
          ageOccupationTab.push([i, attributeTab[occupationTab[i].occSkillPoints[0]]+attributeTab[occupationTab[i].occSkillPoints[2]]*2]);
        }
      } else {
        ageOccupationTab.push([i, attributeTab[occupationTab[i].occSkillPoints[0]]+attributeTab[occupationTab[i].occSkillPoints[1]]*2]);
      }
    } else {
      ageOccupationTab.push([i, attributeTab[occupationTab[i].occSkillPoints[0]] * 4]);
    }
  }
}
ageOccupationTab.sort(([,a],[,b]) => a-b);
ageOccupationTab.splice(0,Math.ceil(ageOccupationTab.length/2));
console.log(ageOccupationTab);

//let playerOccupation = 0;//Math.floor(Math.random()*occupationTab.length);                     //jaki zawód ma nasz badacz
const job = occupationTab[ageOccupationTab[Math.floor(Math.random()*ageOccupationTab.length)][0]];
playerCharacter.occupation = job.name;
console.log(attributeTab);

console.log(job);

if (job.occSkillPoints.length>1) {
  if (job.occSkillPoints.length>2) {
    if (attributeTab[job.occSkillPoints[3]]>attributeTab[job.occSkillPoints[2]]) {
      attributeTab[job.occSkillPoints[2]]=attributeTab[job.occSkillPoints[3]];
    }
    if (attributeTab[job.occSkillPoints[1]]>attributeTab[job.occSkillPoints[2]]) {
      playerSkillPoints = (attributeTab[job.occSkillPoints[0]]+attributeTab[job.occSkillPoints[1]])*2;
    } else {
      playerSkillPoints = (attributeTab[job.occSkillPoints[0]]+attributeTab[job.occSkillPoints[2]])*2;
    }
  } else {
    playerSkillPoints = (attributeTab[job.occSkillPoints[0]]+attributeTab[job.occSkillPoints[1]])*2;
  }
} else {
  playerSkillPoints =attributeTab[job.occSkillPoints[0]] * 4;
}
console.log(playerSkillPoints);
let copySkills = playerCharacter.skills.slice();      //bufor do działania
occSkillListCleaner(copySkills,job.occSkillList);
job.occSkillList.push("credit rating");

let pointTab = [0,0,0,0,0,0,0,0,0];
console.log(job.occSkillList);
console.log(playerCharacter.skills);
//zczytywanie początkowych pkt
for (let i = 0; i < playerCharacter.skills.length; i++) {
  for (let j = 0; j < job.occSkillList.length; j++) {
    if (job.occSkillList[j][0] == playerCharacter.skills[i][0] ||job.occSkillList[j] == playerCharacter.skills[i][0]) {
      
      switch (job.occSkillList[j][0]) {
        case "fighting":
          if (job.occSkillList[j][1] == playerCharacter.skills[i][2]) {
            pointTab[j] = playerCharacter.skills[i][1];
          }
          break;
        case "firearms":
            if (job.occSkillList[j][1] == playerCharacter.skills[i][2]) {
              pointTab[j] = playerCharacter.skills[i][1];
            }
          break;
        case "language":
          if (job.occSkillList[j][1] == playerCharacter.languageOwn) {
            pointTab[j] = playerCharacter.edu;
          } else {
            pointTab[j] = 1;
          }
          
          break;
        default:
          pointTab[j] = playerCharacter.skills[i][1];
          break;
      }
    }
  }
}

//dodawanie punktów z zawodu
pointTab[8] = job.creditRating[0];
console.log(playerSkillPoints);
playerSkillPoints -=job.creditRating[0];
console.log(pointTab);
do {
  let roll =Math.floor(Math.random()*9)
  
    if (roll==8) {
      if (pointTab[roll]<job.creditRating[1]) {
        pointTab[roll]+=1;
        playerSkillPoints-=1;
      }
    } else {
      if (pointTab[roll]<100) {
        pointTab[roll]+=1;
        playerSkillPoints-=1;
      }
    }
} while (playerSkillPoints>0);
console.log(pointTab);

//wrzucanie do postaci punktów z dodanymi z zawodu
let x = playerCharacter.skills.length;    //zmienna która zapamięta początkową ilośc skilli, jak w for sprawdzimy długość to się zapętli dodając nowe skille z art/lan/sci
for (let i = 0; i < x; i++) {
  for (let j = 0; j < job.occSkillList.length; j++) {
    if (job.occSkillList[j][0] == playerCharacter.skills[i][0] || job.occSkillList[j] == playerCharacter.skills[i][0]) {
      
      switch (job.occSkillList[j][0]) {
        case "fighting":
          if (job.occSkillList[j][1] == playerCharacter.skills[i][2]) {
            playerCharacter.skills[i][1] = pointTab[j];
          }
          break;
        case "firearms":
            if (job.occSkillList[j][1] == playerCharacter.skills[i][2]) {
              playerCharacter.skills[i][1] = pointTab[j];
            }
          break;
        case "language":
            playerCharacter.skills.push(["language",pointTab[j],job.occSkillList[j][1]]);
          break;
        case "art/craft":
            playerCharacter.skills.push(["art/craft",pointTab[j],job.occSkillList[j][1]]);
          break;
        case "science":
            playerCharacter.skills.push(["science",pointTab[j],job.occSkillList[j][1]]);
          break;
        default:
          playerCharacter.skills[i][1] = pointTab[j];
          break;
      }
    }
  }
}

//dodanie zainteresowań
let interestsPoint = playerCharacter.int*2;
copySkills = playerCharacter.skills.slice();      //kopia skilli na której będziemy pracować
let interestsTab = [];                            //komora maszyny losującej
let chance = 0;                                   //zmienna która będzie zliczała ile kuponów dostanie dany skill
console.log(copySkills);                          
copySkills.splice(killerSkill,2);                 //usunięcie cr i mythos

generateInterestsTab();

console.log(interestsPoint);
interestsPoint/=10;
console.log(interestsPoint);
let xx = -1;
let z = 0;
let zz = 0;
do {
  if (xx != -1) {
    z = rollD100()
    zz = copySkills[xx][1]*1.5
    if (z>zz) {
      copySkills[xx][1]+=10;
      interestsPoint-=1;
    } else {
      addInterests();
    }
  } else {
      addInterests();
  }
  generateInterestsTab();
  console.log(xx, " umiejka ", copySkills[xx][0],z,zz)
  if (copySkills[xx][1]>100) {
    copySkills[xx][1]-=10;
      interestsPoint+=1;
  }
} while (interestsPoint>0);

console.log(copySkills); 
console.log(playerCharacter.skills);

let addIntSkill = 0;
let countIntSkill = 0;
if (copySkills.length>playerCharacter.skills.length-2) {
  console.log("dodaje umiejke");
  addIntSkill = copySkills.length-(playerCharacter.skills.length-2);
  countIntSkill = addIntSkill;
  for (let q = 0; q < addIntSkill; q++) {
    playerCharacter.skills.push(copySkills[copySkills.length-countIntSkill]);
    countIntSkill--;
  }
}
playerCharacter.skills.sort();
console.log(playerCharacter.skills);
// Make a div
const div = document.createElement('div');
// put it into the body
document.body.appendChild(div);
//wypełnienie atrybutów
function generateCharacter(name, value) {
    const html = `
      <div>
      <p>${name}: ${value} 
      </p>
      </div>
    `;
    return html;
  }
  const atributes = document.createElement('div');
  atributes.classList.add('atributes');
  let atributeHTML = "";
  for (let i = 0; i < 24; i++) {
    atributeHTML += generateCharacter(Object.keys(playerCharacter)[i], Object.values(playerCharacter)[i]); 
  }
  const skillLength = playerCharacter.skills.length;
  let artPoint = 0;
  let languagePoint = 0;
  let fightPoint = 0;
  let firePoint = 0;
  let sciencePoint = 0;
  for (let i = 0; i < skillLength; i++) {       
    if (Object.values(playerCharacter)[24][i][0] == "art/craft" || Object.values(playerCharacter)[24][i][0] == "fighting" || Object.values(playerCharacter)[24][i][0] == "firearms" || Object.values(playerCharacter)[24][i][0] == "language" || Object.values(playerCharacter)[24][i][0] == "science") {
      if (Object.values(playerCharacter)[24][i][0] == "art/craft" && artPoint == 0) {
        atributeHTML += generateCharacter("art/craft","")
        artPoint++;
      }
      if (Object.values(playerCharacter)[24][i][0] == "fighting" && fightPoint == 0) {
        atributeHTML += generateCharacter("fighting","")
        fightPoint++;
      }
      if (Object.values(playerCharacter)[24][i][0] == "firearms" && firePoint == 0) {
        atributeHTML += generateCharacter("firearms","")
        firePoint++;
      }
      if (Object.values(playerCharacter)[24][i][0] == "language" && languagePoint == 0) {
        atributeHTML += generateCharacter("language","")
        languagePoint++;
      }
      if (Object.values(playerCharacter)[24][i][0] == "science" && sciencePoint == 0) {
        atributeHTML += generateCharacter("science","")
        sciencePoint++;
      }
      if (Object.values(playerCharacter)[24][i][2] != "") {
        atributeHTML += generateCharacter(Object.values(playerCharacter)[24][i][2],Object.values(playerCharacter)[24][i][1]);
      }
    } else {
      atributeHTML += generateCharacter(Object.values(playerCharacter)[24][i][0],Object.values(playerCharacter)[24][i][1]);
    }
  }
 
  
  
  atributes.innerHTML = atributeHTML;
  div.insertAdjacentElement('beforebegin', atributes);
  
//rolling D6  
function rollD6() {
  return Math.ceil(Math.random()*6);
}

//rolling D10  
function rollD10() {
  return Math.ceil(Math.random()*10);
}

//rolling D100 
function rollD100() {
  return Math.ceil(Math.random()*100);
}

//checking if Edu skill will be risen
function eduSkillCheck(skill){
  if (rollD100()>skill) {
    return playerCharacter.edu += rollD10();
  } else {
    return false;
  }
}

//age deduct will be bad ater '40
function ageDeduct(deductValue){
  ageDeductFinisher=0;
  for (let i = 0; i < deductValue; i++) {
     let d = Math.floor(Math.random()*3);
     switch (d) {
      case 0:
        if (playerCharacter.str>0) {
          playerCharacter.str -=1;
          break;
        } else {
          ageDeductFinisher ++;
          break;
        }  
      case 1:
        if (playerCharacter.dex>0) {
          playerCharacter.dex -=1;
          break;
        } else {
          ageDeductFinisher ++;
          break;
        }  
      case 2:
        if (playerCharacter.con>0) {
          playerCharacter.con -=1;
          break;
        } else {
          ageDeductFinisher ++;
          break;
        }  
      default:
        break;
     }   
  }
}

// occupation skill list will left with skills ready to upgrade
function occSkillListCleaner(playerSkills,jobSkills){
  playerSkills.splice(killerSkill,2);                //delete Cthulhu Mythos and credit rating

  for (let i = 0; i < jobSkills.length; i++) {
    if (jobSkills[i][0]=="choose") {       // choose one skill from available array
      jobSkills[i] = jobSkills[i][Math.ceil(Math.random()*(jobSkills[i].length-1))];
      switch (jobSkills[i][0]) {
        case "fighting":
          const figX = Math.floor(Math.random()*fightingTab.length);
          jobSkills[i][1] = fightingTab[figX];
          fightingTab.splice(figX,1);
          break;
        case "firearms":
          const firX = Math.floor(Math.random()*firearmsTab.length);
          jobSkills[i][1] = firearmsTab[firX];
          firearmsTab.splice(firX,1);
          break;
        default:
          break;
      } 
    }
    if (jobSkills[i]=="interpersonal") {
        const interp = Math.floor(Math.random()*interpersonalTab.length);
        jobSkills[i] = interpersonalTab[interp];
        interpersonalTab.splice(interp,1);
    }
    if (jobSkills[i]=="other language") {  //choose one language from not known already 
      randomSkillRoll = Math.floor(Math.random()*playerLanguageOwn.length)
      randomSkill = playerLanguageOwn[randomSkillRoll];
      jobSkills[i][0] = "language";
      jobSkills[i][1] = randomSkill;
      playerLanguageOwn.splice(randomSkillRoll,1);
    }
    if (jobSkills[i]=="language") {
      jobSkills[i][0] = "language";
      jobSkills[i][1] = playerCharacter.languageOwn;
    }
    if (jobSkills[i][0]=="art/craft" && jobSkills[i][1] == "random") {  // same as language
      randomSkillRoll = Math.floor(Math.random()*artCraft.length)
      randomSkill = artCraft[randomSkillRoll];
      jobSkills[i][1] = randomSkill;
      artCraft.splice(randomSkillRoll,1);
    }
    if (jobSkills[i][0]=="art/craft" && jobSkills[i][1] != "random") {  
      for (let j = 0; j < artCraft.length; j++) {       //check available art/craft to delete chosen one
        if (artCraft[j] == jobSkills[i][1]) {
          artCraft.splice(j,1);
        } 
      }
    }
    if (jobSkills[i][0]=="science" && jobSkills[i][1] == "random") {
      randomSkillRoll = Math.floor(Math.random()*science.length)
      randomSkill = science[randomSkillRoll];
      jobSkills[i][1] = randomSkill;
      science.splice(randomSkillRoll,1);
    }
    if (jobSkills[i][0]=="science" && jobSkills[i][1] != "random") {
      for (let j = 0; j < science.length; j++) {
        if (science[j] == jobSkills[i][1]) {
          science.splice(j,1);
        } 
      }
    }
    if (jobSkills[i]=="random") {
      randomSkill = Math.floor(Math.random()*playerSkills.length);
        switch (playerSkills[randomSkill][0]) {
          case "art/craft":
            randomSkillRoll = Math.floor(Math.random()*artCraft.length)
            jobSkills[i] = ["art/craft" , artCraft[randomSkillRoll]];
            artCraft.splice(randomSkillRoll,1);
            break;
          case "science":
            randomSkillRoll = Math.floor(Math.random()*science.length)
            jobSkills[i] = ["science" , science[randomSkillRoll]];
            science.splice(randomSkillRoll,1);
            break;
          case "language":
            randomSkillRoll = Math.floor(Math.random()*playerLanguageOwn.length)

            jobSkills[i] = ["language" , playerLanguageOwn[randomSkillRoll]];
            playerLanguageOwn.splice(randomSkillRoll,1);
            break;
          case "firearms":
            jobSkills[i][0] = "firearms";
            jobSkills[i][1] = playerSkills[randomSkill][2];
            break;
          case "fighting":
            jobSkills[i][0] = "fighting";
            jobSkills[i][1] = playerSkills[randomSkill][2];
            break;
        
          default:
            jobSkills[i] = playerSkills[randomSkill][0];
            break;
        }
    }
    for (let j = 0; j < playerSkills.length; j++) {
      if (playerSkills[j][0] == jobSkills[i]) {
        playerSkills.splice(j,1);
      } 
    }
  }
}

function addInterests(){
  xx =  interestsTab[Math.floor(Math.random()*interestsTab.length)];
  console.log(xx);
  switch (xx) {
    case 4:
          randomSkillRoll = Math.floor(Math.random()*artCraft.length)
          randomSkill = artCraft[randomSkillRoll];
          copySkills.push(["art/craft",15,randomSkill]);
          xx = copySkills.length-1;
          artCraft.splice(randomSkillRoll,1);
      break;
    case 21:
        if (Math.random()>0.5) {
          copySkills[xx][1]+=10;
        } else {
          randomSkillRoll = Math.floor(Math.random()*playerLanguageOwn.length)
          randomSkill = playerLanguageOwn[randomSkillRoll];
          copySkills.push(["language",11,randomSkill]);
          xx = copySkills.length-1;
          playerLanguageOwn.splice(randomSkillRoll,1);
        }
      break;
    case 39:
          randomSkillRoll = Math.floor(Math.random()*science.length)
          randomSkill = science[randomSkillRoll];
          copySkills.push(["science",11,randomSkill]);
          xx = copySkills.length-1;
          science.splice(randomSkillRoll,1);
      break;
    default:
        copySkills[xx][1]+=10;
      break;  
  }
  interestsPoint-=1;
}

function generateInterestsTab(){
  interestsTab = [];
for (let i = 0; i < copySkills.length; i++) {     //iteracja po skillach
chance = Math.ceil((100-copySkills[i][1])/10);    //ile kuponów ma dostać skill, 10 podstawa, 1 mniej za każde 10 pkt w umiejce
for (let j = 0; j < chance; j++) {                //iteracja po ilości kuponów
  interestsTab.push(i);                           //generacja kuponu do tabelki
}
}
}